package com.example.filippo.beaconsreceiver;

import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.estimote.sdk.BeaconManager;
import com.estimote.sdk.EstimoteSDK;
import com.estimote.sdk.telemetry.EstimoteTelemetry;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class MainActivity extends AppCompatActivity {

    private BeaconManager beaconManager;
    //Thread-safe set to store the packet and avoid duplicates
    static final Set<BeaconData> buffer = Collections.synchronizedSet(new HashSet<BeaconData>());
    private String scanIDT;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Initialize the beacon manager
        EstimoteSDK.initialize(getApplicationContext(), "plain-e3d", "4180e1971192b9fc330cb6210dc79916");
        beaconManager = new BeaconManager(this);
        beaconManager.setForegroundScanPeriod(200, 0);
        beaconManager.setBackgroundScanPeriod(200, 0);
        beaconManager.setTelemetryListener(new BeaconManager.TelemetryListener() {

            @Override
            public void onTelemetriesFound(List<EstimoteTelemetry> list) {
                //all the packets found while scanning go to the buffer
                for (EstimoteTelemetry e : list) {
                    BeaconData beaconData = new BeaconData(e.deviceId.toString(), e.timestamp.getTime(),
                            e.ambientLight, e.temperature, e.motionState);
                    buffer.add(beaconData);
                }
            }

        });

        //timed upload on db
        ScheduledThreadPoolExecutor executor = new ScheduledThreadPoolExecutor(1);
        executor.scheduleWithFixedDelay(new Runnable() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void run() {
                ObjectMapper mapper = new ObjectMapper();
                try {
                    //Log.d("I/O", "thread ok "+ "buffer size " + buffer.size() +" " +buffer+" "+ "seed1 "  );
                    //synchronization is necessary because of the for
                    synchronized (buffer) {
                        for (BeaconData b : buffer) {
                            Log.d("I/O", "" + b.getId());
                            //According to the id the beacon goes to different collection in the db
                            switch (b.getId()) {
                                case "[56f8732d98f7efc1df6a8f71013db03b]":
                                case "[1089c62b67905a6601546ed23fc4412f]":
                                    PostHTTPData("room1", mapper.writeValueAsString(b));
                                    break;
                                case "[0e5a0468bce35daf93b699c155100007]":
                                case "[0a2f3fb363addf53e2a52156a6b2c82c]":
                                    PostHTTPData("room2", mapper.writeValueAsString(b));
                                    break;
                                case "[b8b59f3615630eab99096f8919980109]":
                                    PostHTTPData("door", mapper.writeValueAsString(b));
                                    break;
                                default:
                                    Log.d("B", "beacon non riconosciuto " + b.getId());
                                    break;
                            }
                        }
                        //we have saved the packets: we can clean the buffer
                        buffer.clear();
                        //Log.d("I/O", "salvataggio ok "+buffer.size()+" "+ "seed1 " );
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    Log.d("I/O", "errore nella post" );
                }
            }
        }, 10, 10, TimeUnit.SECONDS);
    }

    // avvio del servizio
    @Override
    protected void onStart(){
        super.onStart();
        beaconManager.connect(new BeaconManager.ServiceReadyCallback() {
            @Override public void onServiceReady() {
                // questo per i beacon
                scanIDT =  beaconManager.startTelemetryDiscovery();
            }
        });
    }

    @Override
    protected void onStop(){
        super.onStop();
        // questo per i beacon
        beaconManager.stopTelemetryDiscovery(scanIDT);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy(){
        super.onDestroy();
        beaconManager.disconnect();
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void PostHTTPData(String collectionString, String json){
        //connection to the db has to be done through the rest api: http request
        try{
            String urlString = "https://api.mlab.com/api/1/databases/officebeacon/collections/" +
                    collectionString + "?apiKey=ipy9_SrGL9fGC7hIhW5DyWddUBujrDU0";
            URL url = new URL(urlString);
            HttpURLConnection urlConnection = (HttpURLConnection)url.openConnection();

            urlConnection.setRequestMethod("POST");
            urlConnection.setDoOutput(true);

            byte[] out = json.getBytes(StandardCharsets.UTF_8);
            int length = out.length;

            urlConnection.setFixedLengthStreamingMode(length);
            urlConnection.setRequestProperty("Content-Type","application/json; charset=UTF-8");
            urlConnection.connect();
            try(OutputStream os = urlConnection.getOutputStream())
            {
                os.write(out);
            }
            InputStream response = urlConnection.getInputStream();

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
