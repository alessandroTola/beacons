package com.example.filippo.beaconsreceiver;

/**
 * Created by filippo on 27/01/18.
 * Relevant information to be sent to the databases
 */

public class BeaconData {
    private String id;
    private long timestamp;
    private double ambientLight;
    private double temperature;
    private boolean motion;

    public String getId() {
        return id;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public double getAmbientLight() {
        return ambientLight;
    }

    public double getTemperature() {
        return temperature;
    }

    public boolean isMotion() {
        return motion;
    }

    public BeaconData(String id, long timestamp, double ambientLight, double temperature, boolean motion) {
        this.id = id;
        this.timestamp = timestamp;
        this.ambientLight = ambientLight;
        this.temperature = temperature;
        this.motion = motion;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BeaconData that = (BeaconData) o;

        if (timestamp != that.timestamp) return false;
        if (Double.compare(that.ambientLight, ambientLight) != 0) return false;
        if (Double.compare(that.temperature, temperature) != 0) return false;
        if (motion != that.motion) return false;
        return id.equals(that.id);
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = id.hashCode();
        result = 31 * result + (int) (timestamp ^ (timestamp >>> 32));
        temp = Double.doubleToLongBits(ambientLight);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(temperature);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (motion ? 1 : 0);
        return result;
    }

    @Override
    public String toString() {
        return "BeaconData{" +
                "id='" + id + '\'' +
                ", timestamp=" + timestamp +
                ", ambientLight=" + ambientLight +
                ", temperature=" + temperature +
                ", motion=" + motion +
                '}';
    }
}
